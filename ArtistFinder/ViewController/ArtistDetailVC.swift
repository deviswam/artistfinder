//
//  ArtistDetailVC.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class ArtistDetailVC: UIViewController {
    
    @IBOutlet weak var artistImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var albumsCollectionView: UICollectionView!
    
    var albumsCollectionViewDataSource: AlbumsCollectionViewDataSource?
    var albumsCollectionViewDelegateFlowLayout: AlbumsCollectionViewDelegateFlowLayout?
    var viewModel: ArtistDetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.albumsCollectionView.dataSource = albumsCollectionViewDataSource
        self.albumsCollectionView.delegate = albumsCollectionViewDelegateFlowLayout
        self.titleLabel.text = viewModel?.title()
        viewModel?.artistImage(completion: { [weak self] (artistImage) in
            self?.artistImageView.image = artistImage
        })
        viewModel?.getTopAlbumsOfSelectedArtist()
    }
}

extension ArtistDetailVC: ArtistDetailViewModelViewDelegate {
    func albumsLoaded(with result: Result<Void>) {
        switch result {
        case .success:
            self.albumsCollectionView.reloadData()
        case .failure:
            print("Info (Inform user): Unable to fetch albums list")
        }
    }
}

//
//  SearchArtistVC.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class SearchArtistVC: UIViewController {

    @IBOutlet weak var artistSearchBar: UISearchBar!
    @IBOutlet weak var artistsTableView: UITableView!
    
    var artistsTableViewDataSource: UITableViewDataSource?
    var artistsTableViewDelegate: UITableViewDelegate?
    var artistSearchBarDelegate: UISearchBarDelegate?
    var viewModel: SearchArtistViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        artistsTableView.dataSource = artistsTableViewDataSource
        artistsTableView.delegate = artistsTableViewDelegate
        artistSearchBar.delegate = artistSearchBarDelegate
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        artistSearchBar.becomeFirstResponder()
    }
}

extension SearchArtistVC: SearchArtistViewModelViewDelegate {
    func artistsLoaded(with result: Result<Void>) {
        switch result {
        case .success:
            self.artistsTableView.reloadData()
        case .failure:
            print("Info (Inform user): Unable to fetch matching artists list")
        }
    }
}

//
//  SharedComponentsDirectory.swift
//  FoodRatings
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

class SharedComponentsDir {
    static let artistManager: ArtistManager = {
        let artistManager = AFArtistManager(lastFmAPIClient: SharedComponentsDir.lastFmAPIClient)
        return artistManager
    }()
    
    static let lastFmAPIClient: LastFmAPIClient = {
        let lastFmAPIClient = AFLastFmAPIClient()
        return lastFmAPIClient
    }()
}

//
//  ImageStore.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

// This class retrived the photo if it is already cached otherwise downloads and then caches the image.
class ImageStore {
    private static var imageCache = NSCache<NSString, UIImage>()
    static func getImage(with imageUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        let nsImageUrl = imageUrl as NSString
        if let image = imageCache.object(forKey: nsImageUrl) {
            completionHandler(.success(image))
        } else {
            SharedComponentsDir.lastFmAPIClient.photo(with: imageUrl, { (result: Result<UIImage>) in
                switch result {
                case .success(let image):
                    imageCache.setObject(image, forKey: nsImageUrl)
                case .failure(let error):
                    print("Unable to download image. Error: \(error)")
                }
                completionHandler(result)
            })
        }
    }
}

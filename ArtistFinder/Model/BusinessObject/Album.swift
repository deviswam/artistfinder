//
//  Album.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 24/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol Album {
    var name: String { get }
    var id: String? { get }
    var imageURLStr: String? { get }
}

struct AFAlbum: Album, ObjectMapper {
    var name: String
    var id: String?
    var imageURLStr: String?
    
    init(name: String, id: String? = nil, imageURLStr: String? = nil) {
        self.name = name
        self.id = id
        self.imageURLStr = imageURLStr
    }
    
    init?(dictionary: Dictionary<String, Any>?) {
        guard let dictionary = dictionary,
              let dName = dictionary["name"] as? String else {
                return nil
        }
        
        let dId = dictionary["mbid"] as? String
        
        var dImageURLStr: String?
        if var imagesArray = dictionary["image"] as? [[String: AnyObject]] {
            // filter the imagesArray to take out medium image Dictionary
            imagesArray = imagesArray.filter({ imageDictionary -> Bool in
                if let imageSize = imageDictionary["size"] as? String {
                    return imageSize == "medium"
                }
                return false
            })
            
            dImageURLStr = imagesArray.first?["#text"] as? String
        }
        self.init(name: dName, id: dId, imageURLStr: dImageURLStr)
    }
}

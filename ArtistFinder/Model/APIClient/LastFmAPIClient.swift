//
//  LastFmAPIClient.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 24/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

enum LastFmAPIClientError: Error {
    case httpError
    case invalidDataError
    case dataSerializationError
}

protocol LastFmAPIClient {
    // get list of artists matching the name
    func searchArtist(by artistName: String, completionHandler: @escaping (_ result: Result<[Artist]>) -> Void)
    // get list of albums of an artist
    func fetchTopAlbums(of artistName: String, completionHandler: @escaping (_ result: Result<[Album]>) -> Void)
    // downloads the photo from the provided photo url.
    func photo(with photoUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void)
}

class AFLastFmAPIClient: LastFmAPIClient {
    // MARK: PRIVATE VARIABLES
    private let BASE_URL = "http://ws.audioscrobbler.com/2.0/"
    private let commonRequestParams = ["api_key": "c2c32ecdc9daf333f82c0ce79adc4297", "format": "json"]
    
    private let urlSession: URLSession!
    
    // MARK: INITIALIZER
    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }
    
    // MARK: PUBLIC METHODS
    func photo(with photoUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        guard let url = URL(string: photoUrl) else {
            return
        }
        //print("WAM PHOTO URL:\(url.absoluteString)")
        let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10)
        let dataTask = urlSession.dataTask(with: request) { (data, urlResponse, error) in
            var result: Result<UIImage>!
            if error != nil {
                result = .failure(LastFmAPIClientError.httpError)
            } else if let data = data {
                let image = UIImage(data: data)
                if image == nil {
                    result = .failure(LastFmAPIClientError.invalidDataError)
                } else {
                    result = .success(image!)
                }
            }
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
        dataTask.resume()
    }
    
    func searchArtist(by artistName: String, completionHandler: @escaping (_ result: Result<[Artist]>) -> Void){
        let requestParams = ["method": "artist.search",
                             "artist": artistName]
        guard let urlRequest = self.createURLRequest(with: requestParams) else {
            return
        }
        self.getArtists(with: urlRequest) { (result: Result<[Artist]>) in
            completionHandler(result)
        }
    }
    
    func fetchTopAlbums(of artistName: String, completionHandler: @escaping (_ result: Result<[Album]>) -> Void) {
        let requestParams = ["method": "artist.gettopalbums",
                             "artist": artistName]
        guard let urlRequest = self.createURLRequest(with: requestParams) else {
            return
        }
        self.getAlbums(with: urlRequest) { (result: Result<[Album]>) in
            completionHandler(result)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func getAlbums(with urlRequest: URLRequest, completionHandler: @escaping (_ result: Result<[Album]>) -> Void) {
        let dataTask = urlSession.dataTask(with: urlRequest) { [unowned self] (data, urlResponse, error) in
            var result: Result<[Album]>!
            
            if error != nil {
                result = .failure(LastFmAPIClientError.httpError)
            } else if let data = data {
                do {
                    if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                        guard let topAlbums = dictionary["topalbums"] as? [String : AnyObject],
                            let albumsArray = topAlbums["album"] as? [[String: AnyObject]] else {
                                DispatchQueue.main.async {
                                    completionHandler(.failure(LastFmAPIClientError.invalidDataError))
                                }
                                return
                        }
                        result = .success(self.createAlbums(from: albumsArray))
                    }
                } catch {
                    result = .failure(LastFmAPIClientError.dataSerializationError)
                }
            }
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
        dataTask.resume()
    }
    
    private func createAlbums(from albumsArray:[[String : AnyObject]]) -> [Album] {
        let albums = albumsArray.map { (albumDictionary: [String : AnyObject]) -> Album? in
            return AFAlbum(dictionary: albumDictionary)
            }.filter {
                return $0 != nil
            } as! [Album]
        
        return albums
    }
    
    private func getArtists(with urlRequest: URLRequest, completionHandler: @escaping (_ result: Result<[Artist]>) -> Void) {
        let dataTask = urlSession.dataTask(with: urlRequest) { [unowned self] (data, urlResponse, error) in
            var result: Result<[Artist]>!
            
            if error != nil {
                result = .failure(LastFmAPIClientError.httpError)
            } else if let data = data {
                do {
                    if let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                        guard let results = dictionary["results"] as? [String : AnyObject],
                        let artistMatches = results["artistmatches"] as? [String: AnyObject],
                        let artistsArray = artistMatches["artist"] as? [[String: AnyObject]] else {
                            DispatchQueue.main.async {
                                completionHandler(.failure(LastFmAPIClientError.invalidDataError))
                            }
                            return
                        }
                        result = .success(self.createArtists(from: artistsArray))
                    }
                } catch {
                    result = .failure(LastFmAPIClientError.dataSerializationError)
                }
            }
            
            DispatchQueue.main.async {
                 completionHandler(result)
            }
        }
        dataTask.resume()
    }
    
    private func createArtists(from artistsArray:[[String : AnyObject]]) -> [Artist] {
        let artists = artistsArray.map { (artistDictionary: [String : AnyObject]) -> Artist? in
            return AFArtist(dictionary: artistDictionary)
            }.filter { (artist: Artist?) -> Bool in
                return artist != nil
            } as! [Artist]
        
        return artists
    }
    
    private func createURLRequest(with parameters:[String: String] = [:]) -> URLRequest? {
        // create url
        var urlComponents = URLComponents(string: BASE_URL)
        
        // append custom parameters
        var queryItems = [URLQueryItem]()
        for (key, value) in parameters {
            let query = URLQueryItem(name: key, value: value)
            queryItems.append(query)
        }
        // append common params
        commonRequestParams.forEach { queryItems.append(URLQueryItem(name: $0, value: $1))}
        
        urlComponents?.queryItems = queryItems
        guard let url = urlComponents?.url else { return nil }
        //print("WAM URL:\(url.absoluteString)")
        
        // create request object
        let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15)
        
        return request
    }
}

//
//  ArtistManager.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 24/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum ArtistManagerError : Error {
    case noDataFoundError
    case connectionError
    
    func userFriendlyMessage() -> String {
        switch self {
        case .noDataFoundError:
            return "Unable to fetch requested data"
        case .connectionError:
            return "Connection error"
        }
    }
}

protocol ArtistManager {
    // gets list of artists matching the name
    func searchArtist(by artistName: String, completionHandler: @escaping (_ result: Result<[Artist]>) -> Void)
    // gets albums of an artist
    func loadTopAlbums(of artistName: String, completionHandler: @escaping (_ result: Result<[Album]>) -> Void)
}

class AFArtistManager: ArtistManager {
    // MARK: PRIVATE VARIABLES
    private let lastFmAPIClient: LastFmAPIClient
    
    // MARK: INITIALIZER
    init(lastFmAPIClient: LastFmAPIClient) {
        self.lastFmAPIClient = lastFmAPIClient
    }
    
    // MARK: PUBLIC METHODS
    func searchArtist(by artistName: String, completionHandler: @escaping (_ result: Result<[Artist]>) -> Void) {
        lastFmAPIClient.searchArtist(by: artistName) { [unowned self] (result: Result<[Artist]>) in
            var mResult: Result<[Artist]>!
            switch result {
                case .failure(let error):
                    mResult = .failure(self.artistManagerError(from: error as! LastFmAPIClientError))
                case .success(let artists):
                    mResult = .success(artists)
            }
            completionHandler(mResult)
        }
    }
    
    func loadTopAlbums(of artistName: String, completionHandler: @escaping (_ result: Result<[Album]>) -> Void) {
        lastFmAPIClient.fetchTopAlbums(of: artistName) { [unowned self] (result: Result<[Album]>) in
            var mResult: Result<[Album]>!
            switch result {
            case .failure(let error):
                mResult = .failure(self.artistManagerError(from: error as! LastFmAPIClientError))
            case .success(let albums):
                mResult = .success(albums)
            }
            completionHandler(mResult)
        }
    }
    
    // MARK: PRIVATE METHODS
    private func artistManagerError(from apiError: LastFmAPIClientError) -> ArtistManagerError {
        switch apiError {
        case .dataSerializationError, .invalidDataError:
            return ArtistManagerError.noDataFoundError
        case .httpError:
            return ArtistManagerError.connectionError
        }
    }
}

//
//  AlbumCollectionViewCell.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol AlbumCollectionViewCell {
    func configCell(with albumViewModel: AlbumViewModel)
    func setAlbumImage(image: UIImage)
}

class AFAlbumCollectionViewCell: UICollectionViewCell, AlbumCollectionViewCell {
    
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var albumCoverImageView: UIImageView!
    
    func configCell(with albumViewModel: AlbumViewModel) {
        self.albumNameLabel.text = albumViewModel.name
    }
    
    func setAlbumImage(image: UIImage) {
        self.albumCoverImageView.image = image
    }
    
    override func prepareForReuse() {
        self.albumCoverImageView.image = UIImage(named: "album_placeholder")
    }
}

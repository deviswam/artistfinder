//
//  ArtistTableViewCell.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

protocol ArtistTableViewCell {
    func configCell(with artistViewModel: ArtistViewModel)
    func setArtistImage(image: UIImage)
}

class AFArtistTableViewCell: UITableViewCell, ArtistTableViewCell {
    
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var artistImageView: UIImageView!
    
    func configCell(with artistViewModel: ArtistViewModel) {
        artistNameLabel.text = artistViewModel.name
    }
    
    func setArtistImage(image: UIImage) {
        self.artistImageView.image = image
    }
    
    override func prepareForReuse() {
        self.artistImageView.image = UIImage(named: "artist_placeholder")
    }
}

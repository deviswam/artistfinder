//
//  ArtistSearchBarDelegate.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class ArtistSearchBarDelegate: NSObject, UISearchBarDelegate {
    var viewModel: SearchArtistViewModel?
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let viewModel = viewModel, let text = searchBar.text {
            viewModel.searchArtist(by: text)
            searchBar.resignFirstResponder()
        }
    }
}

//
//  ArtistsTableViewDelegate.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class ArtistsTableViewDelegate: NSObject, UITableViewDelegate {
    var viewModel: SearchArtistViewModel?
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.selectedArtist(at: indexPath.row)
    }
}

//
//  ArtistDetailViewModel.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

// MARK:- VIEW DELEGATE PROTOCOL
protocol ArtistDetailViewModelViewDelegate: class {
    func albumsLoaded(with result: Result<Void>)
}

// MARK:- VIEWMODEL PROTOCOL
protocol ArtistDetailViewModel {
    func getTopAlbumsOfSelectedArtist()
    func numberOfAlbums() -> Int
    func album(at index: Int) -> AlbumViewModel?
    func title() -> String
    func artistImage(completion: @escaping (UIImage) -> Void)
}

class AFArtistDetailViewModel: ArtistDetailViewModel {
    // MARK: PRIVATE VARIABLES
    private var selectedArtist: Artist
    private let artistManager: ArtistManager
    private weak var viewDelegate: ArtistDetailViewModelViewDelegate!
    
    // MARK: INITIALIZER
    init(selectedArtist: Artist, artistManager: ArtistManager, viewDelegate: ArtistDetailViewModelViewDelegate) {
        self.selectedArtist = selectedArtist
        self.artistManager = artistManager
        self.viewDelegate = viewDelegate
    }
    
    // MARK: PUBLIC METHODS
    func getTopAlbumsOfSelectedArtist() {
        var vmResult: Result<Void>!
        artistManager.loadTopAlbums(of: self.selectedArtist.name) { [weak self] (result: Result<[Album]>) in
            switch result {
            case .success(let albums):
                self?.selectedArtist.albums = albums
                vmResult = .success()
            case .failure(let error):
                vmResult = .failure(error)
            }
            self?.viewDelegate.albumsLoaded(with: vmResult)
        }
    }
    
    func numberOfAlbums() -> Int {
        guard let albums = self.selectedArtist.albums else {
            return 0
        }
        return albums.count
    }
    
    func album(at index: Int) -> AlbumViewModel? {
        if let albums = self.selectedArtist.albums, index < albums.count {
            return AFAlbumViewModel(album: albums[index])
        }
        return nil
    }
    
    func title() -> String {
        return "\(selectedArtist.name)'s Top Albums"
    }
    
    func artistImage(completion: @escaping (UIImage) -> Void) {
        if let photoUrl = self.selectedArtist.imageURLStr {
            ImageStore.getImage(with: photoUrl) { (result: Result<UIImage>) in
                if case let Result.success(image) = result {
                    completion(image)
                }
            }
        }
    }
}

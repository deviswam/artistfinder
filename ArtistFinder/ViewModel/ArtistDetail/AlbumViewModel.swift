//
//  AlbumViewModel.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol AlbumViewModel {
    var album: Album { get }
    var name: String { get }
}

class AFAlbumViewModel: AlbumViewModel {
    var album: Album
    var name: String = ""
    init(album: Album) {
        self.album = album
        self.name = album.name
    }
}

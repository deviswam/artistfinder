//
//  ArtistViewModel.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 24/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

protocol ArtistViewModel {
    var artist: Artist{ get }
    var name: String { get }
}

class AFArtistViewModel: ArtistViewModel {
    var artist: Artist
    var name: String = ""
    
    init(artist: Artist) {
        self.artist = artist
        self.name = artist.name
    }
}

//
//  SearchArtistViewModel.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 24/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

// MARK:- COORDINATOR DELEGATE PROTOCOL
protocol SearchArtistViewModelCoordinatorDelegate: class {
    func didSelect(artist: Artist)
}

// MARK:- VIEW DELEGATE PROTOCOL
protocol SearchArtistViewModelViewDelegate: class {
    func artistsLoaded(with result: Result<Void>)
}

// MARK:- VIEWMODEL PROTOCOL
protocol SearchArtistViewModel {
    func searchArtist(by name: String)
    func numberOfArtists() -> Int
    func artist(at index: Int) -> ArtistViewModel?
    func selectedArtist(at index: Int)
}

class AFSearchArtistViewModel: SearchArtistViewModel {
    // MARK: PRIVATE VARIABLES
    private let artistManager: ArtistManager
    private weak var viewDelegate: SearchArtistViewModelViewDelegate!
    private weak var coordinatorDelegate: SearchArtistViewModelCoordinatorDelegate!
    private var artists: [Artist]?
    
    // MARK: INITIALIZER
    init(artistManager: ArtistManager, viewDelegate: SearchArtistViewModelViewDelegate, coordinatorDelegate: SearchArtistViewModelCoordinatorDelegate) {
        self.artistManager = artistManager
        self.viewDelegate = viewDelegate
        self.coordinatorDelegate = coordinatorDelegate
    }
    
    // MARK: PUBLIC METHODS
    func searchArtist(by name: String) {
        var vmResult: Result<Void>!
        artistManager.searchArtist(by: name) { [weak self] result in
            switch result {
                case .success(let artists):
                    self?.artists = artists
                    vmResult = .success()
                case .failure(let error):
                    vmResult = .failure(error)
            }
            self?.viewDelegate.artistsLoaded(with: vmResult)
        }
    }
    
    func numberOfArtists() -> Int {
        guard let artists = self.artists else {
            return 0
        }
        return artists.count
    }
    
    func artist(at index: Int) -> ArtistViewModel? {
        if let artists = self.artists, index < artists.count {
            return AFArtistViewModel(artist: artists[index])
        }
        return nil
    }
    
    func selectedArtist(at index: Int) {
        guard let artists = self.artists, index < artists.count else {
            return
        }
        self.coordinatorDelegate.didSelect(artist: artists[index])
    }
}

//
//  ArtistsTableViewDataSource.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class ArtistsTableViewDataSource: NSObject, UITableViewDataSource {
    var viewModel: SearchArtistViewModel?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            return viewModel.numberOfArtists()
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArtistTableViewCell", for: indexPath)
        if let artistCell = cell as? ArtistTableViewCell,
            let artistVM = viewModel?.artist(at: indexPath.row) {
            artistCell.configCell(with: artistVM)
            loadPhoto(with: artistVM.artist.imageURLStr, onCell: artistCell)
        }
        return cell
    }
    
    func loadPhoto(with photoUrl: String?, onCell cell:ArtistTableViewCell) {
        guard let photoUrl = photoUrl else { return }
        ImageStore.getImage(with: photoUrl) { (result: Result<UIImage>) in
            if case let Result.success(image) = result {
                cell.setArtistImage(image: image)
            }
        }
    }
}

//
//  AlbumsCollectionViewDataSource.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class AlbumsCollectionViewDataSource : NSObject, UICollectionViewDataSource {
    var viewModel: ArtistDetailViewModel?
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let viewModel = viewModel {
            return viewModel.numberOfAlbums()
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlbumCollectionViewCell", for: indexPath)
        if let albumCell = cell as? AlbumCollectionViewCell, let albumVM = viewModel?.album(at: indexPath.item) {
            albumCell.configCell(with: albumVM)
            loadPhoto(with: albumVM.album.imageURLStr, onCell: albumCell)
        }
        return cell
    }
    
    func loadPhoto(with photoUrl: String?, onCell cell: AlbumCollectionViewCell) {
        guard let photoUrl = photoUrl else { return }
        ImageStore.getImage(with: photoUrl) { (result: Result<UIImage>) in
            if case let Result.success(image) = result {
                cell.setAlbumImage(image: image)
            }
        }
    }
}

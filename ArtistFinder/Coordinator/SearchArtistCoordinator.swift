//
//  ListAuthoritiesCoordinator.swift
//  FoodRatings
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class SearchArtistCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    fileprivate var navigationController: UINavigationController
    fileprivate var artistDetailCoordinator: ArtistDetailCoordinator?
    
    // MARK:- INITIALIZER
    init(navController: UINavigationController) {
        self.navigationController = navController
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        if let searchArtistVC = navigationController.topViewController as? SearchArtistVC {
            let artistManager = SharedComponentsDir.artistManager
            let artistsTableViewDataSource = ArtistsTableViewDataSource()
            let artistsTableViewDelegate = ArtistsTableViewDelegate()
            let artistSearchBarDelegate = ArtistSearchBarDelegate()
            let searchArtistViewModel = AFSearchArtistViewModel(artistManager: artistManager, viewDelegate: searchArtistVC, coordinatorDelegate: self)
            
            searchArtistVC.viewModel = searchArtistViewModel
            searchArtistVC.artistsTableViewDataSource = artistsTableViewDataSource
            searchArtistVC.artistsTableViewDelegate = artistsTableViewDelegate
            searchArtistVC.artistSearchBarDelegate = artistSearchBarDelegate
            artistsTableViewDataSource.viewModel = searchArtistViewModel
            artistsTableViewDelegate.viewModel = searchArtistViewModel
            artistSearchBarDelegate.viewModel = searchArtistViewModel
        }
    }
}

extension SearchArtistCoordinator: SearchArtistViewModelCoordinatorDelegate {
    func didSelect(artist: Artist) {
        artistDetailCoordinator = ArtistDetailCoordinator(navController: self.navigationController, selectedArtist: artist)
        artistDetailCoordinator?.start()
    }
}

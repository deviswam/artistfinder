//
//  AppCoordinator.swift
//  FoodRatings
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class AppCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    private var window: UIWindow
    private var searchArtistCoordinator : SearchArtistCoordinator?
    
    // MARK:- INITIALIZER
    init(window: UIWindow) {
        self.window = window
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let rootNavController = storyboard.instantiateInitialViewController() as? UINavigationController {
            self.window.rootViewController = rootNavController
            self.showSearchArtist(rootNavController: rootNavController)
        }
    }
    
    // MARK:- PRIVATE METHODS
    private func showSearchArtist(rootNavController: UINavigationController) {
        searchArtistCoordinator = SearchArtistCoordinator(navController: rootNavController)
        searchArtistCoordinator?.start()
    }
}



//
//  ArtistDetailCoordinator.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import UIKit

class ArtistDetailCoordinator : Coordinator {
    // MARK:- PRIVATE VARIABLES
    fileprivate var navigationController: UINavigationController
    private var selectedArtist: Artist
    
    // MARK:- INITIALIZER
    init(navController: UINavigationController, selectedArtist: Artist) {
        self.navigationController = navController
        self.selectedArtist = selectedArtist
    }
    
    // MARK:- PUBLIC METHODS
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let artistDetailVC = storyboard.instantiateViewController(withIdentifier: "ArtistDetailVC") as? ArtistDetailVC {
            let artistManager = SharedComponentsDir.artistManager
            let albumsCollectionViewDataSource = AlbumsCollectionViewDataSource()
            let albumsCollectionViewDelegateFlowLayout = AlbumsCollectionViewDelegateFlowLayout()
            let artistDetailViewModel = AFArtistDetailViewModel(selectedArtist: self.selectedArtist, artistManager: artistManager, viewDelegate: artistDetailVC)
            
            artistDetailVC.viewModel = artistDetailViewModel
            artistDetailVC.albumsCollectionViewDataSource = albumsCollectionViewDataSource
            artistDetailVC.albumsCollectionViewDelegateFlowLayout = albumsCollectionViewDelegateFlowLayout
            albumsCollectionViewDataSource.viewModel = artistDetailViewModel
            self.navigationController.pushViewController(artistDetailVC, animated: true)
        }
    }
}

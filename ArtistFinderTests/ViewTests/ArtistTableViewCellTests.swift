//
//  ArtistTableViewCellTests.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import ArtistFinder

//MARK: MOCKS & FAKES
extension ArtistTableViewCellTests {
    class FakeDataSource: NSObject, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return AFArtistTableViewCell()
        }
    }
    
    class FakeArtistViewModel: ArtistViewModel {
        var name: String { return "cher"}
        var artist: Artist { return AFArtist(name: "") }
    }
}

class ArtistTableViewCellTests: XCTestCase {
    let fakeDataSource = FakeDataSource()
    var tableView : UITableView!
    var cell : AFArtistTableViewCell!
    
    override func setUp() {
        super.setUp()
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let searchArtistVC = storyBoard.instantiateViewController(withIdentifier: "SearchArtistVC") as! SearchArtistVC
        searchArtistVC.artistsTableViewDataSource = fakeDataSource
        UIApplication.shared.keyWindow?.rootViewController = searchArtistVC
        _ = searchArtistVC.view
        tableView = searchArtistVC.artistsTableView
        cell = tableView.dequeueReusableCell(withIdentifier: "ArtistTableViewCell", for: IndexPath(row: 0, section: 0)) as! AFArtistTableViewCell
    }
    
    func testArtistCell_HasNameLabel() {
        // Assert
        XCTAssertNotNil(cell.artistNameLabel)
    }
    
    func testArtistCell_HasArtistImageView() {
        // Assert
        XCTAssertNotNil(cell.artistImageView)
    }
    
    func testConfigCell_SetNameOnArtistNameLabel() {
        //Act
        cell.configCell(with: FakeArtistViewModel())
        
        //Assert
        XCTAssertEqual(cell.artistNameLabel.text!, "cher")
    }
}

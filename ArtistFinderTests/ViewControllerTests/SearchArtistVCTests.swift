//
//  SearchArtistVCTests.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import ArtistFinder

extension SearchArtistVCTests {
    class MockSearchArtistViewModel: SearchArtistViewModel {
        var artists: [Artist]?
        func artist(at index: Int) -> ArtistViewModel? {
            return nil
        }
        
        func numberOfArtists() -> Int {
            return artists!.count
        }
        
        var searchArtistName: String?
        func searchArtist(by name: String) {
            searchArtistName = name
        }
        
        func selectedArtist(at index: Int) {
        }
    }
    
    class MockArtistSearchBarDelegate: ArtistSearchBarDelegate {
        override func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            viewModel?.searchArtist(by: searchBar.text!)
        }
    }
}

class SearchArtistVCTests: XCTestCase {
    var sut: SearchArtistVC!
    
    override func setUp() {
        super.setUp()
        
        // Arrange
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        sut = storyBoard.instantiateViewController(withIdentifier: "SearchArtistVC") as! SearchArtistVC
    }
    
    func testLocationSearchBar_whenViewIsLoaded_isNotNil() {
        // Act
        _ = sut.view
        // Assert
        XCTAssertNotNil(sut.artistSearchBar)
    }
    
    func testArtistsTableView_whenViewIsLoaded_isNotNil() {
        //Act
        _ = sut.view
        // Assert
        XCTAssertNotNil(sut.artistsTableView)
    }
    
    func testArtistSearchBar_whenViewIsLoaded_hasDelegate() {
        // Arrange
        sut.artistSearchBarDelegate = ArtistSearchBarDelegate()
        // Act
        _ = sut.view
        // Assert
        XCTAssert(sut.artistSearchBar.delegate is ArtistSearchBarDelegate)
    }
    
    func testArtistsTableView_whenViewIsLoaded_hasDataSource() {
        // Act
        sut.artistsTableViewDataSource = ArtistsTableViewDataSource()
        _ = sut.view
        // Assert
        XCTAssert(sut.artistsTableViewDataSource is ArtistsTableViewDataSource)
    }
    
    func testArtistsTableView_whenViewIsLoaded_hasDelegate() {
        // Arrange
        sut.artistsTableViewDelegate = ArtistsTableViewDelegate()
        // Act
        _ = sut.view
        // Assert
        XCTAssert(sut.artistsTableViewDelegate is ArtistsTableViewDelegate)
    }
    
    func testSearchArtist_whenSearchButtonIsPressed_shouldAskViewModelForArtistSearch() {
        // Arrange
        let mockViewModel = MockSearchArtistViewModel()
        let mockArtistSearchBarDelegate = MockArtistSearchBarDelegate()
        mockArtistSearchBarDelegate.viewModel = mockViewModel
        let fakeSearchBar = UISearchBar()
        fakeSearchBar.text = "cher"
        
        // Act
        mockArtistSearchBarDelegate.searchBarSearchButtonClicked(fakeSearchBar)
        
        // Assert
        XCTAssertEqual(mockViewModel.searchArtistName, "cher")
    }
}

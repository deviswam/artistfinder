//
//  ArtistsTableViewDataSourceTests.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import ArtistFinder

extension ArtistsTableViewDataSourceTests {
    class MockTableView: UITableView {
        var cellGotDequeued = false
        override func dequeueReusableCell(withIdentifier identifier: String, for indexPath: IndexPath) -> UITableViewCell {
            cellGotDequeued = true
            return super.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        }
    }
    
    class MockArtistTableViewCell: UITableViewCell, ArtistTableViewCell {
        var configCellGotCalled = false
        var artist : ArtistViewModel?
        func configCell(with artistViewModel: ArtistViewModel) {
            configCellGotCalled = true
            self.artist = artistViewModel
        }
        func setArtistImage(image: UIImage) {
        }
    }
    
    class MockSearchArtistViewModel: SearchArtistViewModel {
        var artists: [Artist]?
        func artist(at index: Int) -> ArtistViewModel? {
            return AFArtistViewModel(artist: artists![index])
        }
        
        func numberOfArtists() -> Int {
            return artists!.count
        }
        
        func searchArtist(by name: String) {
        }
        
        func selectedArtist(at index: Int) {
            
        }
    }
}

class ArtistsTableViewDataSourceTests: XCTestCase {
    func testDataSourceHasSearchArtistViewModel() {
        // Arrange
        let sut = ArtistsTableViewDataSource()
        sut.viewModel = MockSearchArtistViewModel()
        // Assert
        XCTAssertNotNil(sut.viewModel)
        XCTAssertNotNil((sut.viewModel! as Any) is SearchArtistViewModel)
    }
    
    func testNumberOfRowsInTheSection_WithTwoArtists_ShouldReturnTwo() {
        //Arrange
        let sut = ArtistsTableViewDataSource()
        let artists = [AFArtist(name: "cher"), AFArtist(name: "mike")]
        let mockSearchArtistViewModel = MockSearchArtistViewModel()
        mockSearchArtistViewModel.artists = artists
        
        sut.viewModel = mockSearchArtistViewModel
        let mockTableView = MockTableView()
        
        //Act
        let noOfItemsInSectionZero = sut.tableView(mockTableView, numberOfRowsInSection: 0)
        
        //Assert
        XCTAssertEqual(noOfItemsInSectionZero, 2, "Number of items in section 0 are 2")
    }
    
    func testCellForRow_ReturnsArtistTableViewCell() {
        //Arrange
        let sut = ArtistsTableViewDataSource()
        let artists = [AFArtist(name: "cher"), AFArtist(name: "mike")]
        let mockSearchArtistViewModel = MockSearchArtistViewModel()
        mockSearchArtistViewModel.artists = artists
        
        sut.viewModel = mockSearchArtistViewModel
        let mockTableView = MockTableView(frame: CGRect.zero)
        
        mockTableView.dataSource = sut
        mockTableView.register(MockArtistTableViewCell.self, forCellReuseIdentifier: "ArtistTableViewCell")
        
        //Act
        let cell = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(cell is ArtistTableViewCell,"Returned cell is of ArtistTableViewCell type")
    }
    
    func testCellForRow_DequeuesCell() {
        //Arrange
        let sut = ArtistsTableViewDataSource()
        let artists = [AFArtist(name: "cher"), AFArtist(name: "mike")]
        let mockSearchArtistViewModel = MockSearchArtistViewModel()
        mockSearchArtistViewModel.artists = artists
        
        sut.viewModel = mockSearchArtistViewModel
        let mockTableView = MockTableView(frame: CGRect.zero)
        
        mockTableView.dataSource = sut
        mockTableView.register(MockArtistTableViewCell.self, forCellReuseIdentifier: "ArtistTableViewCell")
        
        //Act
        _ = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0))
        
        //Assert
        XCTAssertTrue(mockTableView.cellGotDequeued,"CellForRow should be calling DequeueCell method")
    }
    
    func testConfigCell_GetsCalledFromCellForRow() {
        //Arrange
        let sut = ArtistsTableViewDataSource()
        let artists = [AFArtist(name:"cher", id: "123")]
        let mockSearchArtistViewModel = MockSearchArtistViewModel()
        mockSearchArtistViewModel.artists = artists
        
        sut.viewModel = mockSearchArtistViewModel
        let mockTableView = MockTableView(frame: CGRect.zero)
        
        mockTableView.dataSource = sut
        mockTableView.register(MockArtistTableViewCell.self, forCellReuseIdentifier: "ArtistTableViewCell")
        
        //Act
        let cell = sut.tableView(mockTableView, cellForRowAt: IndexPath(item: 0, section: 0)) as! MockArtistTableViewCell
        
        //Assert
        XCTAssertTrue(cell.configCellGotCalled,"CellForRow should be calling ConfigCell method")
        XCTAssertTrue(cell.artist?.name == artists.first?.name, "Artist name should be same")
    }

}

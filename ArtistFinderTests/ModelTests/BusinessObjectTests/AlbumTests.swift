//
//  AlbumTests.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 24/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import ArtistFinder

class AlbumTests: XCTestCase {
    func testConformanceToAlbumProtocol() {
        // Arrange
        let sut = AFAlbum(name: "Thriller")
        // Assert
        XCTAssert((sut as Any) is Album)
    }
    
    func testInit_albumMustBeCreatedWithAName() {
        // Arrange
        let albumName = "Thriller"
        let sut = AFAlbum(name: albumName)
        // Assert
        XCTAssertEqual(sut.name, albumName)
    }
    
    func testInit_albumShouldHaveAnId() {
        // Arrange
        let albumId = "bfcc6d75-a6a5-4bc6-8282-47aec8531818"
        let sut = AFAlbum(name: "Thriller", id: albumId)
        // Assert
        XCTAssertEqual(sut.id, albumId)
    }
    
    func testInit_albumShouldHaveAImageURLStr() {
        // Arrange
        let albumImageURLStr = "https://lastfm-img2.akamaized.net/i/u/64s/4c2467672b39f5f94750eaf4da155383.png"
        let sut = AFAlbum(name: "Thriller", imageURLStr: albumImageURLStr)
        // Assert
        XCTAssertEqual(sut.imageURLStr, albumImageURLStr)
    }
    
    func testAlbumConformsToObjectMapperProtocol() {
        //Arrange
        let sut = AFAlbum(name: "Thriller")
        
        //Assert
        XCTAssertTrue((sut as Any) is ObjectMapper, "Album should conforms to ObjectMapper protocol")
    }
    
    func testInit_withNilDictionary_shouldFailInitialization() {
        //Arrange
        let sut = AFAlbum(dictionary: nil)
        
        //Assert
        XCTAssertNil(sut, "Album should be created only with valid json dictionary")
    }
    
    func testInit_withNoAlbumNameInDictionary_shouldFailInitialization() {
        //Arrange
        let jsonAlbumDictionary = ["mbid":"bfcc6d75-a6a5-4bc6-8282-47aec8531818"]
        let sut = AFAlbum(dictionary: jsonAlbumDictionary)
        
        //Assert
        XCTAssertNil(sut, "Album should only be created if dictionary has name")
    }
    
    func testInit_withNoMBIDInDictionary_shouldNotFailInitialization() {
        //Arrange
        let jsonAlbumDictionary = ["name":"Thriller"]
        let sut = AFAlbum(dictionary: jsonAlbumDictionary)
        
        //Assert
        XCTAssertNotNil(sut, "Only name is mandatory to create Album object")
    }
    
    func testInit_withNoImageURLInDictionary_shouldNotFailInitialization() {
        //Arrange
        let jsonAlbumDictionary = ["name":"Thriller"]
        let sut = AFAlbum(dictionary: jsonAlbumDictionary)
        
        //Assert
        XCTAssertNotNil(sut, "Only name is mandatory to create Album object")
    }
    
    func testInit_withAlbumNameAndMBIDAndImageURLInDictionary_shouldSetAlbumNameIdAndImageURL() {
        //Arrange
        let imageArray = [["#text": "https://lastfm-img2.akamaized.net/i/u/64s/4c2467672b39f5f94750eaf4da155383.png", "size":"medium"]]
        let jsonAlbumDictionary = ["mbid":"bfcc6d75-a6a5-4bc6-8282-47aec8531818", "name":"Thriller", "image": imageArray] as [String: Any]
        let sut = AFAlbum(dictionary: jsonAlbumDictionary)
        
        //Assert
        XCTAssertEqual(sut?.id, "bfcc6d75-a6a5-4bc6-8282-47aec8531818")
        XCTAssertEqual(sut?.name, "Thriller")
        XCTAssertEqual(sut?.imageURLStr, "https://lastfm-img2.akamaized.net/i/u/64s/4c2467672b39f5f94750eaf4da155383.png")
    }

}

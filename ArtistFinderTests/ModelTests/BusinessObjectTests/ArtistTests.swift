//
//  ArtistTests.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 24/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import ArtistFinder

class ArtistTests: XCTestCase {
    func testConformanceToArtistProtocol() {
        // Arrange
        let sut = AFArtist(name: "cher")
        // Assert
        XCTAssert((sut as Any) is Artist)
    }
    
    func testInit_artistMustBeCreatedWithAName() {
        // Arrange
        let artistName = "cher"
        let sut = AFArtist(name: artistName)
        // Assert
        XCTAssertEqual(sut.name, artistName)
    }
    
    func testInit_artistShouldHaveAnId() {
        // Arrange
        let artistId = "bfcc6d75-a6a5-4bc6-8282-47aec8531818"
        let sut = AFArtist(name: "cher", id: artistId)
        // Assert
        XCTAssertEqual(sut.id, artistId)
    }
    
    func testInit_artistShouldHaveAImageURLStr() {
        // Arrange
        let artistImageURLStr = "https://lastfm-img2.akamaized.net/i/u/64s/4c2467672b39f5f94750eaf4da155383.png"
        let sut = AFArtist(name: "cher", imageURLStr: artistImageURLStr)
        // Assert
        XCTAssertEqual(sut.imageURLStr, artistImageURLStr)
    }
    
    func testInit_artistShouldHaveAlbumCollection() {
        // Arrange
        let albums = [AFAlbum(name: "Thriller")]
        var sut = AFArtist(name: "cher")
        sut.albums = albums
        // Assert
        XCTAssertNotNil(sut.albums)
        XCTAssertEqual(sut.albums?.first?.name, "Thriller")
    }
    
    func testArtistConformsToObjectMapperProtocol() {
        //Arrange
        let sut = AFArtist(name: "cher")
        
        //Assert
        XCTAssertTrue((sut as Any) is ObjectMapper, "Artist should conforms to ObjectMapper protocol")
    }
    
    func testInit_withNilDictionary_shouldFailInitialization() {
        //Arrange
        let sut = AFArtist(dictionary: nil)
        
        //Assert
        XCTAssertNil(sut, "Artist should be created only with valid json dictionary")
    }
    
    func testInit_withNoArtistNameInDictionary_shouldFailInitialization() {
        //Arrange
        let jsonArtistDictionary = ["mbid":"bfcc6d75-a6a5-4bc6-8282-47aec8531818"]
        let sut = AFArtist(dictionary: jsonArtistDictionary)
        
        //Assert
        XCTAssertNil(sut, "Artist should only be created if dictionary has name")
    }
    
    func testInit_withNoMBIDInDictionary_shouldNotFailInitialization() {
        //Arrange
        let jsonArtistDictionary = ["name":"cher"]
        let sut = AFArtist(dictionary: jsonArtistDictionary)
        
        //Assert
        XCTAssertNotNil(sut, "Only name is mandatory to create Artist object")
    }
    
    func testInit_withNoImageURLInDictionary_shouldNotFailInitialization() {
        //Arrange
        let jsonArtistDictionary = ["name":"cher"]
        let sut = AFArtist(dictionary: jsonArtistDictionary)
        
        //Assert
        XCTAssertNotNil(sut, "Only name is mandatory to create Artist object")
    }
    
    func testInit_withArtistNameAndMBIDAndImageURLInDictionary_shouldSetArtistNameIdAndImageURL() {
        //Arrange
        let imageArray = [["#text": "https://lastfm-img2.akamaized.net/i/u/64s/4c2467672b39f5f94750eaf4da155383.png", "size":"large"]]
        let jsonArtistDictionary = ["mbid":"bfcc6d75-a6a5-4bc6-8282-47aec8531818", "name":"Cher", "image": imageArray] as [String: Any]
        let sut = AFArtist(dictionary: jsonArtistDictionary)
        
        //Assert
        XCTAssertEqual(sut?.id, "bfcc6d75-a6a5-4bc6-8282-47aec8531818")
        XCTAssertEqual(sut?.name, "Cher")
        XCTAssertEqual(sut?.imageURLStr, "https://lastfm-img2.akamaized.net/i/u/64s/4c2467672b39f5f94750eaf4da155383.png")
    }
}

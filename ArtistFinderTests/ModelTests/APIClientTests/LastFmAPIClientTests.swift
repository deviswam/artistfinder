//
//  LastFmAPIClientTests.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 25/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import ArtistFinder

extension LastFmAPIClientTests {
    class MockURLSession: URLSession {
        typealias Handler = (Data?, URLResponse?, Error?) -> Void
        
        var dataTaskRequestMethodCalled = false
        var urlRequest : URLRequest?
        var completionHandler : Handler?
        var dataTask = MockURLSessionDataTask()
        
        override func dataTask(with urlRequest: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            dataTaskRequestMethodCalled = true
            self.urlRequest = urlRequest
            self.completionHandler = completionHandler
            return dataTask
        }
    }
    
    class MockURLSessionDataTask : URLSessionDataTask {
        var resumeGotCalled = false
        override func resume() {
            resumeGotCalled = true
        }
    }
}

class LastFmAPIClientTests: XCTestCase {
    var mockURLSession: MockURLSession!
    var sut: AFLastFmAPIClient!
    
    override func setUp() {
        super.setUp()
        mockURLSession = MockURLSession()
        sut = AFLastFmAPIClient(urlSession: mockURLSession)
    }
    
    func testConformanceToLastFmAPIClientProtocol() {
        XCTAssertTrue((sut as AnyObject) is LastFmAPIClient)
    }
    
    func testSearchArtistByName_ShouldAskURLSessionForFetchingMatchedArtistsData() {
        // Act
        sut.searchArtist(by: "cher") { result in
            
        }
        
        // Assert
        XCTAssert(mockURLSession.dataTaskRequestMethodCalled)
    }
    
    func testSearchArtistByName_WithValidJSONOfAnArtist_ShouldReturnAnArtistObject() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let localAuthoritiesAPIResponseInString = "{\"results\": {\"artistmatches\": {" +
        "\"artist\":[{" +
            "\"mbid\":\"123\"," +
            "\"name\":\"cher\"" +
        "}]}}}"
        
        let responseData = localAuthoritiesAPIResponseInString.data(using: .utf8)
        let expectedArtistId = "123"
        var receivedArtist : Artist?
        var receivedError : LastFmAPIClientError?
        
        //Act
        sut.searchArtist(by: "cher") { (result: Result<[Artist]>) in
            switch result {
            case .success(let artists):
                receivedArtist = artists.first
            case .failure(let error):
                receivedError = error as? LastFmAPIClientError
            }
            asynExpectation.fulfill()
        }
        
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedArtist?.id == expectedArtistId, "Fetched and expected artist should have same id")
            XCTAssertTrue(receivedArtist?.name == "cher", "Fetched and expected artist should have same name")
            XCTAssertNil(receivedError, "Nil should be returned as an Error along with artist object")
        }
    }
    
    func testSearchArtistByName_WithValidJSONOfTwoArtists_ShouldReturnTwoArtistObjects() {
        // Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        let localAuthoritiesAPIResponseInString = "{\"results\": {\"artistmatches\": {" +
            "\"artist\":[{" +
            "\"mbid\":\"123\"," +
            "\"name\":\"cher\"" +
            "},{" +
            "\"mbid\":\"423\"," +
            "\"name\":\"cherl\"" +
        "}]}}}"
        
        let responseData = localAuthoritiesAPIResponseInString.data(using: .utf8)
        var receivedArtists : [Artist]?
        var receivedError : LastFmAPIClientError?
        
        //Act
        sut.searchArtist(by: "cher") { (result: Result<[Artist]>) in
            switch result {
            case .success(let artists):
                receivedArtists = artists
            case .failure(let error):
                receivedError = error as? LastFmAPIClientError
            }
            asynExpectation.fulfill()
        }
        
        mockURLSession.completionHandler!(responseData, nil, nil)
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertTrue(receivedArtists?[0].id == "123", "Fetched and expected artist should have same id")
            XCTAssertTrue(receivedArtists?[0].name == "cher", "Fetched and expected artist should have same name")
            XCTAssertTrue(receivedArtists?[1].id == "423", "Fetched and expected artist should have same id")
            XCTAssertTrue(receivedArtists?[1].name == "cherl", "Fetched and expected artist should have same name")
            XCTAssertNil(receivedError, "Nil should be returned as an Error along with artist object")
        }
    }
}

//
//  ArtistManagerTests.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 24/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import ArtistFinder

extension ArtistManagerTests {
    class MockLastFmAPIClient: LastFmAPIClient {
        var searchArtistCalled = false
        var completionHandler: ((Result<[Artist]>) -> Void)?
        func searchArtist(by name: String, completionHandler: @escaping (_ result: Result<[Artist]>) -> Void){
            searchArtistCalled = true
            self.completionHandler = completionHandler
        }
        func photo(with photoUrl: String, _ completionHandler: @escaping (Result<UIImage>) -> Void) {
        }
        
        func fetchTopAlbums(of artistName: String, completionHandler: @escaping (Result<[Album]>) -> Void) {
            
        }
    }
}

class ArtistManagerTests: XCTestCase {
    var mockLastFmAPIClient: MockLastFmAPIClient!
    var sut: AFArtistManager!
    
    override func setUp() {
        super.setUp()
        mockLastFmAPIClient = MockLastFmAPIClient()
        sut = AFArtistManager(lastFmAPIClient: mockLastFmAPIClient)
    }
    
    func testConformanceToArtistManagerProtocol() {
        // Assert
        XCTAssertTrue((sut as AnyObject) is ArtistManager, "AFArtistManager should conforms to ArtistManager protocol")
    }
    
    func testSearchArtistByName_asksLastFmAPIClientToFetchMatchingArtists() {
        // Act
        sut.searchArtist(by: "cher") { (result: Result<[Artist]>) in
            
        }
        
        // Assert
        XCTAssert(mockLastFmAPIClient.searchArtistCalled)
    }
    
    func testSearchArtistByName_whenArtistsFound_shouldReceiveArtistsCollectionWithNoError() {
        // Arrange
        var receivedArtists : [Artist]?
        var receivedError : Error?
        let expectedArtists = [AFArtist(name: "cher")]
        
        // Act
        sut.searchArtist(by: "cher") { (result: Result<[Artist]>) in
            switch result {
            case .success(let artists):
                receivedArtists = artists
            case .failure(let error):
                receivedError = error
            }
        }
        mockLastFmAPIClient.completionHandler?(.success(expectedArtists))
        
        // Assert
        XCTAssertNil(receivedError)
        XCTAssertNotNil(receivedArtists)
        XCTAssert(expectedArtists.first! == receivedArtists!.first!)
    }
}

//
//  SearchArtistViewModelTests.swift
//  ArtistFinder
//
//  Created by Waheed Malik on 24/05/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import XCTest
@testable import ArtistFinder

extension SearchArtistViewModelTests {
    class MockArtistManager: ArtistManager {
        var searchedArtistName: String?
        var completionHandler: ((Result<[Artist]>) -> Void)?
        func searchArtist(by name: String, completionHandler: @escaping (_ result: Result<[Artist]>) -> Void) {
            self.searchedArtistName = name
            self.completionHandler = completionHandler
        }
        func loadTopAlbums(of artistName: String, completionHandler: @escaping (Result<[Album]>) -> Void) {
            
        }
    }
    
    class MockSearchArtistViewModelViewDelegate: SearchArtistViewModelViewDelegate {
        var expectation: XCTestExpectation?
        var artistsViewModelResult: Result<Void>?
        
        func artistsLoaded(with result: Result<Void>) {
            guard let asynExpectation = expectation else {
                XCTFail("MockSearchArtistViewModelViewDelegate was not setup correctly. Missing XCTExpectation reference")
                return
            }
            self.artistsViewModelResult = result
            asynExpectation.fulfill()
        }
    }
    
    class MockSearchArtistViewModelCoordinatorDelegate: SearchArtistViewModelCoordinatorDelegate {
        func didSelect(artist: Artist) {
        }
    }
}

class SearchArtistViewModelTests: XCTestCase {
    var sut: AFSearchArtistViewModel!
    var mockArtistManager: MockArtistManager!
    var mockViewDelegate: MockSearchArtistViewModelViewDelegate!
    var mockCoordinatorDelegate: MockSearchArtistViewModelCoordinatorDelegate!
    
    override func setUp() {
        super.setUp()
        mockArtistManager = MockArtistManager()
        mockViewDelegate = MockSearchArtistViewModelViewDelegate()
        mockCoordinatorDelegate = MockSearchArtistViewModelCoordinatorDelegate()
        sut = AFSearchArtistViewModel(artistManager: mockArtistManager, viewDelegate: mockViewDelegate, coordinatorDelegate: mockCoordinatorDelegate)
    }
    
    func testConformanceToSearchArtistViewModelProtocol() {
        // Assert
        XCTAssertTrue((sut as AnyObject) is SearchArtistViewModel, "AFSearchArtistViewModel should conforms to SearchArtistViewModel protocol")
    }
    
    func testSearchArtistByName_shouldAskArtistManagerForListOfMatchingArtists() {
        // Act
        sut.searchArtist(by:"cher")
        // Assert
        XCTAssertEqual(mockArtistManager.searchedArtistName, "cher")
    }
    
    func testSearchArtistByName_whenMatchingArtistsFound_raisesArtistsLoadedDelegateCallbackWithSuccess() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchArtist(by:"cher")
        mockArtistManager.completionHandler?(Result.success([AFArtist(name: "cher", id: "123")]))
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            
            switch self.mockViewDelegate.artistsViewModelResult! {
                case .success():
                    XCTAssert(true)
                default:
                    XCTFail("View delegate should only receive success result when artists found")
            }
        }
    }
    
    func testSearchArtistByName_whenNoMatchingArtistsFound_raisesArtistsLoadedDelegateCallbackWithFailure() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchArtist(by:"cher")
        mockArtistManager.completionHandler?(Result.failure(ArtistManagerError.noDataFoundError))
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            
            switch self.mockViewDelegate.artistsViewModelResult! {
            case .success():
                XCTFail("View delegate should receive failure with an error when NO artists found")
            case .failure(let error):
                XCTAssertEqual(error as! ArtistManagerError, ArtistManagerError.noDataFoundError)
            }
        }
    }
    
    func testNumberOfArtists_whenNoArtistsFound_returnsZero() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchArtist(by:"cher")
        mockArtistManager.completionHandler?(Result.failure(ArtistManagerError.noDataFoundError))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfArtists(), 0)
        }
    }
    
    func testNumberOfArtists_whenOneArtistFound_returnsOne() {
        // Arrange
        let artists = [AFArtist(name: "cher")]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchArtist(by:"cher")
        mockArtistManager.completionHandler?(Result.success(artists))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfArtists(), 1)
        }
    }
    
    func testNumberOfArtists_whenFourArtistsFound_returnsFour() {
        // Arrange
        let artists = [AFArtist(name: "cher"), AFArtist(name: "cherl"), AFArtist(name: "bruno"), AFArtist(name: "mars")]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchArtist(by:"cher")
        mockArtistManager.completionHandler?(Result.success(artists))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertEqual(self.sut.numberOfArtists(), 4)
        }
    }
    
    func testArtistAtIndex_returnsTheCorrectArtistViewModelObject() {
        // Arrange
        let artists = [AFArtist(name: "cher", id: "123"), AFArtist(name: "cherl", id: "453")]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchArtist(by:"cher")
        mockArtistManager.completionHandler?(Result.success(artists))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let artist1 = self.sut.artist(at: 0)
            let artist2 = self.sut.artist(at: 1)
            XCTAssertEqual(artist1?.name, "cher")
            XCTAssertEqual(artist2?.name, "cherl")
        }
    }
    
    func testArtistAtIndex_whenArtistNotFoundOnPassedIndex_returnsNil() {
        // Arrange
        let artists = [AFArtist(name: "cher", id: "123"), AFArtist(name: "cherl", id: "453")]
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.searchArtist(by:"cher")
        mockArtistManager.completionHandler?(Result.success(artists))
        
        // Assert
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            let artist = self.sut.artist(at: 2)
            XCTAssertNil(artist)
        }
    }
}

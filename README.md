IMPORTANT NOTES
===============

1. The project fully implements the requested user story with the acceptance criteria.
2. iOS 9 onwards supported. Tested on iPhone 5 onwards devices in both orientation. 
3. The project has been developed on latest xcode 8.3.2 without any warnings or errors.
4. The app is following MVVM-C (Model-View-ViewModel-Coordinator) architecture pattern. 
5. Test driven development approach has been adopted by writting test first with XCTest. Only suitable test cases have been written considering the time limitation and effort involved. Project Test suite has 51 test cases covering most of the core application components.
6. I preferred to use all the built-in native iOS technologies and not using third-party libraries or CocoaPods for this small project.
8. With more time, following things could be implemented/improved:
	* Aiming to achieve 100% test coverage by writing test cases for artist detail screen and its related MVVM-C module components. Most of those test cases would be same as search artist screen MVVM-C module, whose test cases are already written.
    * Error handling unit tests as i was aiming to complete the happy path first. Despite of that application error handling code is stable and app doesn't crash on httpError, serialization scenarios or invalid data for example.
	* Test cases are not refactored to follow DRY principle hence there would be some repetition in few test cases.
	* Loading more records when user scrolls at the end of the list by using pagination of the lastFmAPI.
    * User info messages are currently being shown in Xcode console incase of any issue with retriving data, this could be improved by showing alert to the user.
9. This project is built on best software engineering practices. To name a few: SOLID principles, composition over inheritance, protocol oriented programming, loosely coupled architecture and TDD
10. Please turn on the Software keyboard in the simulator, if the keyboard doesn't appear itself on search artist screen.

Thanks for your time.
